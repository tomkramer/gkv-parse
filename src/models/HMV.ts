/* eslint-disable unicorn/filename-case */
export interface GkvCatalogJson {
    HMV_GRUPPE: GkvCatalogGruppe[];
    HMV_ORT: GkvCatalogOrt[];
    HMV_UNTERGRUPPE: GkvCatalogUntergruppe[];
    HMV_ART: GkvCatalogArt[];
    HMV_PRODUKT: GkvCatalogProdukt[];
}

export interface GkvCatalogGruppe {
    GRUPPE: string;
    BEZEICHNUNG: string;
    DEFINITION: string;
    INDIKATION: string;
    QUERVERWEISE: string;
}

export interface GkvCatalogOrt {
    ORT: string;
    BEZEICHNUNG: string;
}

export interface GkvCatalogUntergruppe {
    GRUPPE: string;
    ORT: string;
    UNTERGRUPPE: string;
    BEZEICHNUNG: string;
    ANFORDERUNGEN: string;
}

export interface GkvCatalogArt {
    GRUPPE: string;
    ORT: string;
    UNTERGRUPPE: string;
    ART: string;
    BEZEICHNUNG: string;
    BESCHREIBUNG: string;
    INDIKATION: string;
}

export interface GkvCatalogProdukt {
    GRUPPE: string;
    ORT: string;
    UNTERGRUPPE: string;
    ART: string;
    PRODUKT: string;
    BEZEICHNUNG: string;
    HERSTELLER: string;
    MERKMALE: string;
}
