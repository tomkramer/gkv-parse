import { Command, flags } from '@oclif/command';
import * as fs from 'fs';
import { GkvCatalogJson, GkvCatalogGruppe, GkvCatalogProdukt, GkvCatalogOrt, GkvCatalogArt, GkvCatalogUntergruppe } from '../models/HMV';
import JSZip = require('jszip');
import * as xml2js from 'xml2js';
import { stripPrefix } from 'xml2js/lib/processors';
import cli from 'cli-ux';

export default class Hmv extends Command {

  static description = 'Parses the specified medical aids (Hilfmittel) catalog XML file and writes the JSON representation of each entry into single files.'

  static examples = [
    `$ gkv-parse hmv -d ~/Desktop/20200331_HMV.xml
`,
  ]

  static flags = {
    help: flags.help({ char: 'h' }),
    dir: flags.string({ char: 'd', description: 'Target directory.', default: './' }),
    zipCompressionLevel: flags.integer({ description: 'Zip compression level.', default: 5, options: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] }),
    // file: flags.string({ char: 'f', description: 'XML-based Hilfsmittel file catalog to parse.', default: './' }),
  }

  static args = [{ name: 'file', required: true, description: 'XML-based Hilfsmittel file catalog to parse.' }]

  static KEY_DELIMITER = '_';

  static NUMBER_DELIMITER = '.';


  async run(): Promise<void> {

    const { args, flags } = this.parse(Hmv)

    const outDir = flags.dir;
    const xmlFile: string = args.file;
    const parseDate: number = Date.now();
    const compressionLevel: number = flags.zipCompressionLevel;

    this.log(`Parsing Hilfsmittelverzeichnis from ${xmlFile} to ${outDir}`)

    if (!fs.existsSync(xmlFile)) {
      throw new Error(`Could not find specified file ${xmlFile}`);
    }
    if (!outDir || !fs.existsSync(outDir)) {
      throw new Error(`Specified output directory ${outDir} does not exist.`);
    }

    const results = await this.parseGkvProducts(xmlFile) as GkvCatalogJson;

    const zip = new JSZip();

    cli.action.start(`Adding ${results.HMV_GRUPPE.length} groups [HMV_GRUPPE] `);
    results.HMV_GRUPPE.forEach((item: GkvCatalogGruppe) => {
      const key = this.generateGroupKey(item);
      const meta = { documentType: 'MedicalAidGroup', documentKey: key, createdAt: parseDate, changedAt: parseDate};
      const content = JSON.stringify(Object.assign({meta: meta}, item));      
      zip.file(`HMV_GRUPPE/${key}.json`, content);
    });
    cli.action.stop('done');


    cli.action.start(`Add ${results.HMV_ORT.length} locations [HMV_ORT] `);
    results.HMV_ORT.forEach((item: GkvCatalogOrt) => {
      const key = this.generateLocationKey(item);
      const meta = { documentType: 'MedicalAidLocation', documentKey: key, createdAt: parseDate, changedAt: parseDate};
      const content = Object.assign({meta: meta}, item);
      zip.file(`HMV_ORT/${key}.json`, JSON.stringify(content));
    });
    cli.action.stop('done');

    cli.action.start(`Add ${results.HMV_UNTERGRUPPE.length} subgroups [HMV_UNTERGRUPPE] `); 
    results.HMV_UNTERGRUPPE.forEach((item: GkvCatalogUntergruppe) => {
      const key = this.generateSubgroupKey(item);
      const refs = this.generateSubgroupRefs(item);   
      const meta = { documentType: 'MedicalAidSubgroup', documentKey: key, createdAt: parseDate, changedAt: parseDate};
      const content = Object.assign({meta: meta, refs: refs}, item);
      zip.file(`HMV_UNTERGRUPPE/${key}.json`, JSON.stringify(content));
    });
    cli.action.stop('done');

    cli.action.start(`Add ${results.HMV_ART.length} types [HMV_ART] `); 
    results.HMV_ART.forEach((item: GkvCatalogArt) => {
      const key = this.generateTypeKey(item);
      const refs = this.generateTypeRefs(item);
      const meta = { documentType: 'MedicalAidType', documentKey: key, createdAt: parseDate, changedAt: parseDate};
      const content = Object.assign({meta: meta, refs: refs}, item);
      zip.file(`HMV_ART/${key}.json`, JSON.stringify(content));
    });
    cli.action.stop('done');

    cli.action.start(`Add ${results.HMV_PRODUKT.length} products [HMV_PRODUKT] `); 
    results.HMV_PRODUKT.forEach((item: GkvCatalogProdukt) => {
      const key = this.generateProductKey(item);
      const refs = this.generateProductRefs(item);
      const hmnr = this.generateMedicalAidNumber(item);
      const meta = { documentType: 'MedicalAidProduct', documentKey: key, rsDate: parseDate};
      const content = Object.assign({meta: meta, refs: refs, HMNR: hmnr}, item);
      zip.file(`HMV_PRODUKT/${key}.json`,JSON.stringify( content));
    });
    cli.action.stop('done');

    cli.action.start('Compress & write zip file ');
    zip
      .generateNodeStream({ type: 'nodebuffer', streamFiles: true, compression: 'DEFLATE', compressionOptions: { level:  compressionLevel} })
      .pipe(fs.createWriteStream(`${outDir}/gkv-json.zip`))
      .on('finish', function () {
        cli.action.stop('done');
        console.log(`gkv-json.zip was written to ${outDir}`);
      });

  }

  // stripPrefix: strips the xml namespace prefix, e.g <hv:GRUPPE/> will become 'GRUPPE'
  // explicitRoot=false: do not get the root node (here "hv:HMV") in the resulting object
  private async parseGkvProducts(xmlPath: string): Promise<any> {
    try {
      return Promise.resolve()
        .then(() => cli.action.start('Parsing HMV '))
        .then(() => fs.readFileSync(xmlPath, 'utf-8'))
        .then((xml) => xml2js.parseStringPromise(xml, { tagNameProcessors: [stripPrefix], explicitArray: false, explicitRoot: false }))
        .finally(() => cli.action.stop('done.'))
    } catch (e) {
      throw new Error(e);
    }
  }

  private generateGroupKey(medicalEquipment: GkvCatalogGruppe): string {
    return 'MedicalAidGroup' + Hmv.KEY_DELIMITER
      + String(medicalEquipment.GRUPPE).padStart(2, '0');
  }

  private generateLocationKey(medicalEquipment: GkvCatalogOrt): string {
    return 'MedicalAidLocation' + Hmv.KEY_DELIMITER
      + String(medicalEquipment.ORT).padStart(2, '0');
  }

  private generateSubgroupKey(medicalEquipment: GkvCatalogUntergruppe): string {
    return 'MedicalAidSubgroup' + Hmv.KEY_DELIMITER
      + String(medicalEquipment.GRUPPE).padStart(2, '0')
      + String(medicalEquipment.ORT).padStart(2, '0')
      + String(medicalEquipment.UNTERGRUPPE).padStart(2, '0');
  }

  private generateTypeKey(medicalEquipment: GkvCatalogArt): string {
    return 'MedicalAidType' + Hmv.KEY_DELIMITER
      + String(medicalEquipment.GRUPPE).padStart(2, '0')
      + String(medicalEquipment.ORT).padStart(2, '0')
      + String(medicalEquipment.UNTERGRUPPE).padStart(2, '0')
      + medicalEquipment.ART;
  }

  private generateProductKey(medicalEquipment: GkvCatalogProdukt): string {
    return 'MedicalAidProduct' + Hmv.KEY_DELIMITER
      + String(medicalEquipment.GRUPPE).padStart(2, '0')
      + String(medicalEquipment.ORT).padStart(2, '0')
      + String(medicalEquipment.UNTERGRUPPE).padStart(2, '0')
      + medicalEquipment.ART
      + String(medicalEquipment.PRODUKT).padStart(3, '0');
  }

  private generateMedicalAidNumber(medicalEquipment: GkvCatalogProdukt): string {
    return String(medicalEquipment.GRUPPE).padStart(2, '0') + Hmv.NUMBER_DELIMITER
      + String(medicalEquipment.ORT).padStart(2, '0') + Hmv.NUMBER_DELIMITER
      + String(medicalEquipment.UNTERGRUPPE).padStart(2, '0') + Hmv.NUMBER_DELIMITER
      + medicalEquipment.ART
      + String(medicalEquipment.PRODUKT).padStart(3, '0');
  }

  private generateSubgroupRefs(medicalEquipment: GkvCatalogUntergruppe): unknown {
    return {
      medicalAidGroup: 'MedicalAidGroup' + Hmv.KEY_DELIMITER + String(medicalEquipment.GRUPPE).padStart(2, '0'),
      medicalAidLocation: 'MedicalAidLocation' + Hmv.KEY_DELIMITER + String(medicalEquipment.ORT).padStart(2, '0')
    };
  }

  private generateTypeRefs(medicalEquipment: GkvCatalogArt): unknown {
    return {
      medicalAidGroup: 'MedicalAidGroup' + Hmv.KEY_DELIMITER + String(medicalEquipment.GRUPPE).padStart(2, '0'),
      medicalAidLocation: 'MedicalAidLocation' + Hmv.KEY_DELIMITER + String(medicalEquipment.ORT).padStart(2, '0'),
      medicalAidSubgroup: 'MedicalAidSubgroup' + Hmv.KEY_DELIMITER + String(medicalEquipment.GRUPPE).padStart(2, '0') + String(medicalEquipment.ORT).padStart(2, '0') + String(medicalEquipment.UNTERGRUPPE).padStart(2, '0')
    };
  }

  private generateProductRefs(medicalEquipment: GkvCatalogProdukt): unknown {
    return {
      medicalAidGroup: 'MedicalAidGroup' + Hmv.KEY_DELIMITER + String(medicalEquipment.GRUPPE).padStart(2, '0'),
      medicalAidLocation: 'MedicalAidLocation' + Hmv.KEY_DELIMITER + String(medicalEquipment.ORT).padStart(2, '0'),
      medicalAidSubgroup: 'MedicalAidSubgroup' + Hmv.KEY_DELIMITER + String(medicalEquipment.GRUPPE).padStart(2, '0') + String(medicalEquipment.ORT).padStart(2, '0') + String(medicalEquipment.UNTERGRUPPE).padStart(2, '0'),
      medicalAidType: 'MedicalAidType' + Hmv.KEY_DELIMITER + String(medicalEquipment.GRUPPE).padStart(2, '0') + String(medicalEquipment.ORT).padStart(2, '0') + String(medicalEquipment.UNTERGRUPPE).padStart(2, '0') + medicalEquipment.ART
    };    
  }

}
