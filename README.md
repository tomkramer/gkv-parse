gkv-parse
==========

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/gkv-parser.svg)](https://npmjs.org/package/gkv-parser)
[![Downloads/week](https://img.shields.io/npm/dw/gkv-parser.svg)](https://npmjs.org/package/gkv-parser)
[![License](https://img.shields.io/npm/l/gkv-parser.svg)](https://github.com/CHAABP/gkv-parser/blob/master/package.json)

Parses entries from the medical aids catalog (Hilfsmittelverzeichnis) of the German statutory health insurances to respective representation in json documents.

At time of writing [this was the current version - 03.04.2020](https://www.gkv-datenaustausch.de/media/dokumente/leistungserbringer_1/sonstige_leistungserbringer/positionsnummernverzeichnisse/20200403_HMV.zip) of the medical aids catalog. But check the [data exchange website]( ) for newer ones.

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g gkv-parse
$ gkv-parse COMMAND
running command...
$ gkv-parse (-v|--version|version)
gkv-parse/0.2.0 darwin-x64 node-v12.14.1
$ gkv-parse --help [COMMAND]
USAGE
  $ gkv-parse COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`gkv-parse help [COMMAND]`](#gkv-parse-help-command)
* [`gkv-parse hmv FILE`](#gkv-parse-hmv-file)

## `gkv-parse help [COMMAND]`

display help for gkv-parse

```
USAGE
  $ gkv-parse help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.3/src/commands/help.ts)_

## `gkv-parse hmv FILE`

Parses the specified medical aids (Hilfmittel) catalog XML file and writes the JSON representation of each entry into single files.

```
USAGE
  $ gkv-parse hmv FILE

ARGUMENTS
  FILE  XML-based Hilfsmittel file catalog to parse.

OPTIONS
  -d, --dir=dir                              [default: ./] Target directory.
  -h, --help                                 show CLI help
  --zipCompressionLevel=0|1|2|3|4|5|6|7|8|9  [default: 5] Zip compression level.

EXAMPLE
  $ gkv-parse hmv -d ~/Desktop/20200331_HMV.xml
```
<!-- commandsstop -->

Local test run:
```
./bin/run hmv -d ~/ ~/Desktop/20200331_HMV.xml
```
